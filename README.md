# Itroduction

An implementation of picture rectification based on the theory of the book
_Multiple View Geometry in Computer Vision_, second edition, by Richard Hartley
and Andrew Zisserman, Cambridge University Press. Pass a picture to this Python
script, then mark 4 points that are known to be rectangular in Euclidean space
and watch the magic!

# Dependencies

* [Python 2.7](http://www.python.org)
* [NumPy](http://www.numpy.org) (>= 1.11.0) - Array/matrix manipulation routines
* [OpenCV](http://opencv.org) (>= 2.4) - Image loading, saving and display

# Improvements

* Bilinear and bicubic filtering
* Allow choosing to map either the whole image or just a part of it (current state)
* Allow specifying dimensions of mapped rectangle
* Command-line switch to allow saving the mapped image
* Local optimization to improve location of marked points (stick to actual image edges)
* Draw lines as points are marked by the user
* Undo option in user interface

# Acknowledgements

To Richard Hartley and Andrew Zisserman for writing a book on multiple view
geometry that makes sense!

# Contact

* [E-mail](mailto:pasad@cos.ufrj.br)
* [Web page](http://pedroasad.com)
* [Gitlab profile](https://gitlab.com/u/psa-exe)
