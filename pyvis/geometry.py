# -*- coding: utf-8 -*-
"""\
========================
Core geometric utilities
========================
"""

import numpy as np
import numpy.linalg as npl

def unitline(p1, p2):
    """Computes a line in projective coordinates with unit normal vector.
    
    Parameters
    ----------
        p1 : sequence
            Length 3 sequence of homogeneous coordinates representing a point.
        p2 : sequence
            Length 3 sequence of homogeneous coordinates representing a point.
            
    Returns
    -------
        l : :class:`numpy.ndarray`
            Homogeneous representation of the projective line that passes
            through points ``p1`` and ``p2``.            
    """
    l = np.cross(p1, p2)
    k = npl.norm(l[:2])
    return l / k