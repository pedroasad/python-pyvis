#!/usr/bin/env python2
# coding: utf-8
"""\
===================================================================
Removing perspective distortion from a perspective image of a plane
===================================================================

Image rectification algorithm based on rectangular point selection. The theory 
behind this algorithm is laid out in Example 2.12 of the book

    Multiple View Geometry in Computer Vision, 2nd edition
    by Richard Hartley, Andrew Zisserman
    Cambridge University Press
    
The core function used by this program is :func:`pyvis.methods.fourPointsPlanarRectification`.
"""

import cv2
import geometry as geo
import itertools as it
import logging as log
import numpy as np
import os
import sys

from argparse import ArgumentParser, FileType
from pyvis.methods import fourPointsPlanarRectification

class Application(object):
    def __init__(self, args):
        self.args = args
        self.points = []
        self.mindist = 10
        
        self.origImage = cv2.imread(self.args.input.name)
        self.origImage = cv2.cvtColor(self.origImage, cv2.COLOR_BGR2BGRA)
        self.destImage = None
        
        self.frontend = FrontendGUI(self)
        
    def addPoint(self, p):
        if len(self.points) < 4:
            lines = [
                geo.unitline(a, b)
                for a, b in it.combinations(self.points, 2)
            ]
            if all(np.dot(l, p) > self.mindist for l in lines):
                self.points.append(p)
                return True
            else:
                log.warn('Refusing point %s: closer than %s to previously inserted points' % (p, self._mindist))
        return False
        
    def run(self):
        self.frontend.collectPoints()
        
        if len(self.points) < 4:
            log.error('Exit requested before enough points were marked. I quit!')
            sys.exit(1)
        
        self.destImage = fourPointsPlanarRectification(
            self.origImage, 
            np.array(self.points)[:,:2], 
            (args.height, args.width)
        )
    
        #==========================================================================
        log.info('Displaying results')
        #==========================================================================
        self.frontend.handleResult()
        
        if self.args.output.name != os.devnull:
            cv2.imwrite(self.args.output.name, self.destImage)
            
        log.info('Done! Bye')
        
class FrontendGUI(object):
    def __init__(self, app):
        self.app = app
        self.maintitle = 'Mark 4 rectangular points'
        
        cv2.namedWindow(self.maintitle)
        cv2.setMouseCallback(self.maintitle, self.mouseCallback)
        
    def checkQuit(self):
        key = int(cv2.waitKey(100) & 255)
        return key in [ord('q'), ord('Q'), 27]
        
    def collectPoints(self):
        self.updateDisplay()
        
        while self.checkQuit() == False and len(self.app.points) < 4:
            self.updateDisplay()
        
        self.updateDisplay()
        
        cv2.setMouseCallback(self.maintitle, lambda *args: None)
        
    def handleResult(self):
        cv2.imshow('Result', self.app.destImage)
        while not self.checkQuit():
            continue
        cv2.destroyAllWindows()
    
    def mouseCallback(self, event, x, y, *args):
        if event == cv2.EVENT_LBUTTONUP:
            p = (x, y, 1)
            self.app.addPoint(p)
                
    def updateDisplay(self):
        hardColor = (0, 255, 0, 255)
        dispImage = np.copy(self.app.origImage)
        points = self.app.points       
        
        for (x,y,z) in points:
            cv2.circle(dispImage, (x, y), 3, hardColor, -1)
            
        if len(points) >= 2:
            cv2.line(dispImage, points[0][:2], points[1][:2], hardColor)
        if len(points) == 3:
            cv2.line(dispImage, points[1][:2], points[2][:2], hardColor)
            cv2.line(dispImage, points[2][:2], points[0][:2], hardColor)
        elif len(points) == 4:
            cv2.line(dispImage, points[1][:2], points[2][:2], hardColor)
            cv2.line(dispImage, points[2][:2], points[3][:2], hardColor)
            cv2.line(dispImage, points[3][:2], points[0][:2], hardColor)
            
        cv2.imshow(self.maintitle, dispImage)

if __name__ == '__main__':
    #==========================================================================
    # Command-line parsing
    #==========================================================================    
    parser = ArgumentParser(description=__doc__)

    parser.add_argument('input',
        type=FileType('r'),
        help='Path to source image'
    )
    parser.add_argument('width',
        type=int,
        help='Width (in pixels) of marked image patch after rectification'
    )
    parser.add_argument('height',
        type=int,
        help='Height (in pixels) of marked image patch after rectification'
    )
    parser.add_argument('--output', '-o',
        type=FileType('w'), default=os.devnull,
        help='Path to output image'
    )
    parser.add_argument('--verbose', '-v', 
        action='count', default=0,
        help='Detailed information on algorithm execution (double for debugging)'
    )
    
    args = parser.parse_args()
    args.verbose = min(max(0, args.verbose), 2)
    
    Application(args).run()