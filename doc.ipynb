{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n",
    "\n",
    "> Based on __Example 2.12__ of the text book.\n",
    "\n",
    "Given a point $\\mathbf{x}^\\intercal$ in world coordinates and the respective point $\\mathbf{x}^\\prime$ in image coordinates, we define $H$ as the world→image projective transformation such that\n",
    "\n",
    "$$\n",
    "\\mathbf{x}^\\prime = H \\mathbf{x}\n",
    "$$\n",
    "\n",
    "We may obtain image-space coordinates $(x'', y'')^\\intercal$ by applying the world→image projection $H$ and dividing by the homogenizing coordinate, as follows:\n",
    "\n",
    "$$\n",
    "x'' = \\frac\n",
    "    {h_{11} x + h_{12} y + h_{13}}\n",
    "    {h_{31} x + h_{32} y + h_{33}}, \n",
    "\\quad\n",
    "y'' = \\frac\n",
    "    {h_{21} x + h_{22} y + h_{23}}\n",
    "    {h_{31} x + h_{32} y + h_{33}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solving for the coefficients of $H$\n",
    "\n",
    "Each pair of point correspondences $(\\mathbf{x}, \\mathbf{x}'')$ will give one equation of each form, which may be rearranged in terms of the unknown matrix coefficients $h_{ij}$, giving us pairs of equations\n",
    "\n",
    "$$\n",
    "\\left[\\begin{matrix}\n",
    "    x & y & 1 & 0 & 0 & 0 & -xx'' & -yx'' & -x'' \\\\\n",
    "    0 & 0 & 0 & x & y & 1 & -xy'' & -yy'' & -y''\n",
    "\\end{matrix}\\right]\n",
    "\\mathbf{h}_9 = 0\n",
    "$$\n",
    "\n",
    "with\n",
    "\n",
    "$$\n",
    "\\mathbf{h}_9^\\intercal = \n",
    "\\left[\\begin{matrix}\n",
    "    h_{11} & h_{12} & h_{13} & \n",
    "    h_{21} & h_{22} & h_{23} & \n",
    "    h_{31} & h_{32} & h_{33}\n",
    "\\end{matrix}\\right]\n",
    "$$\n",
    "\n",
    "Since the overall scaling is unimportant, there are actually only 8 relevant degrees of freedom to estimate, so we may drop one of the coefficients from the $\\mathbf{h}_9$ vector, say $h_{33}$, and make equal to 1, obtaining\n",
    "\n",
    "$$\n",
    "\\left[\\begin{matrix}\n",
    "    x & y & 1 & 0 & 0 & 0 & -xx'' & -yx'' \\\\\n",
    "    0 & 0 & 0 & x & y & 1 & -xy'' & -yy'' \n",
    "\\end{matrix}\\right]\n",
    "\\mathbf{h}_8 = \\left[\\begin{matrix}\n",
    "    x'' \\\\ y'' \n",
    "\\end{matrix}\\right]\n",
    "$$\n",
    "\n",
    "with \n",
    "\n",
    "$$\n",
    "\\mathbf{h}_8^\\intercal = \n",
    "\\left[\\begin{matrix}\n",
    "    h_{11} & h_{12} & h_{13} & \n",
    "    h_{21} & h_{22} & h_{23} & \n",
    "    h_{31} & h_{32}\n",
    "\\end{matrix}\\right]\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary\n",
    "\n",
    "With the above formulation, 4 non-colinear point correspondences $(\\mathbf{x}_i, \\mathbf{x}_i')$ are sufficient to estimate $\\mathbf{h}_8$ as the solution of\n",
    "\n",
    "$$\n",
    "\\left[\\begin{matrix}\n",
    "    x_{1} & y_{1} & 1 & 0 & 0 & 0 & -x_{1}x_{1}'' & -y_{1}x_{1}'' \\\\\n",
    "    0 & 0 & 0 & x_{1} & y_{1} & 1 & -x_{1}y_{1}'' & -y_{1}y_{1}'' \\\\\n",
    "    x_{2} & y_{2} & 1 & 0 & 0 & 0 & -x_{2}x_{2}'' & -y_{2}x_{2}'' \\\\\n",
    "    0 & 0 & 0 & x_{2} & y_{2} & 1 & -x_{2}y_{2}'' & -y_{2}y_{2}'' \\\\\n",
    "    x_{3} & y_{3} & 1 & 0 & 0 & 0 & -x_{3}x_{3}'' & -y_{3}x_{3}'' \\\\\n",
    "    0 & 0 & 0 & x_{3} & y_{3} & 1 & -x_{3}y_{3}'' & -y_{3}y_{3}'' \\\\  \n",
    "    x_{4} & y_{4} & 4 & 0 & 0 & 0 & -x_{4}x_{4}'' & -y_{4}x_{4}'' \\\\\n",
    "    0 & 0 & 0 & x_{4} & y_{4} & 4 & -x_{4}y_{4}'' & -y_{4}y_{4}''\n",
    "\\end{matrix}\\right]\n",
    "\\left[\\begin{matrix}\n",
    "    h_{11} \\\\ h_{12} \\\\ h_{13} \\\\ \n",
    "    h_{21} \\\\ h_{22} \\\\ h_{23} \\\\\n",
    "    h_{31} \\\\ h_{32}\n",
    "\\end{matrix}\\right]\n",
    "=\n",
    "\\left[\\begin{matrix}\n",
    "    x_1'' \\\\ y_1'' \\\\\n",
    "    x_2'' \\\\ y_2'' \\\\\n",
    "    x_3'' \\\\ y_3'' \\\\\n",
    "    x_4'' \\\\ y_4''\n",
    "\\end{matrix}\\right]\n",
    "$$\n",
    "\n",
    "In the next section, we describe different strategies for choosing the reference world points $\\mathbf{x}_i$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Image adjustment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The world coordinates $\\mathbf{x}_i$ corresponding to marked image points $\\mathbf{x}_i'$ are not known in advance, but we may choose convenient values to start with:\n",
    "\n",
    "* $\\mathbf{x}_1' = H\\,(0, 0)^\\intercal$ — upper left marked image point\n",
    "* $\\mathbf{x}_2' = H\\,(0, 1)^\\intercal$ — bottom left marked image point\n",
    "* $\\mathbf{x}_3' = H\\,(1, 0)^\\intercal$ — upper right marked image point\n",
    "* $\\mathbf{x}_4' = H\\,(1, 1)^\\intercal$ — bottom right marked image point\n",
    "\n",
    "This ordering is convenient for implementation, because it is trivial to sort the marked points first by $x$, then by $y$ coordinates in most programming languages. With these points, the restriction matrices are fully specified and we may solve for $\\mathbf{h}_8$ by simply multiplying the right-hand side, on the left, by the inverse of the $8 \\times 8$ matrix on the left-hand side. \n",
    "\n",
    "The $H$ matrix that we obtain this way maps the unit square onto the distorted rectangle marked in the image. When producing an undistorted picture we, in fact, need to deal with issues such as:\n",
    "\n",
    "* What is the actual aspect ratio of the marked rectangle?\n",
    "* What are the dimensions of the mapped picture?\n",
    "* Are we to undistort just the marked rectangle?\n",
    "    * If not, what will we do with target pixels that are mapped off any source pixels?\n",
    "    \n",
    "While the first is an affine ambiguity that cannot be determined without further information, the others are mostly choices that depend on the requirements of the application. We address some of these issues in the following sections."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Only interest region\n",
    "\n",
    "At first, let us assume that only the marked region is relevant and that it should have dimensions $w \\times h$. Just change the world reference points to\n",
    "\n",
    "* $\\mathbf{x}_1 = (0, 0)$\n",
    "* $\\mathbf{x}_2 = (0, h)$\n",
    "* $\\mathbf{x}_3 = (w, 0)$\n",
    "* $\\mathbf{x}_4 = (w, h)$\n",
    "\n",
    "and compute $H$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Whole image\n",
    "\n",
    "Assuming we computed $H$ to map the unit square into the distorted plane, we may compute $H^{-1}$ and use it to find the world coordinates of the image corners as follows\n",
    "\n",
    "* $\\mathbf{x}_{UL} = H^{-1} (0, 0)^\\intercal$\n",
    "* $\\mathbf{x}_{BL} = H^{-1} (0, h)^\\intercal$\n",
    "* $\\mathbf{x}_{UR} = H^{-1} (w, 0)^\\intercal$\n",
    "* $\\mathbf{x}_{BR} = H^{-1} (w, h)^\\intercal$\n",
    "\n",
    "But in the most general setting, $H^{-1}$ will not map these image corners into a rectangle in world coordinates, so we need to figure out a transformation that maps these points into the range $D_W = \\left[0,w\\right]\\times\\left[0,h\\right]$ of world coordinates [[1]](#footnote1). We can do this by appending a transformation of the form \n",
    "\n",
    "$$\n",
    "S = \n",
    "\\left[ \\begin{matrix}\n",
    "    w / s_x & 0 & 0 \\\\\n",
    "    0 & h / s_y & 0 \\\\\n",
    "    0 & 0 & 1\n",
    "\\end{matrix} \\right]\n",
    "\\left[ \\begin{matrix}\n",
    "    1 & 0 & -t_x \\\\\n",
    "    0 & 1 & -t_y \\\\\n",
    "    0 & 0 &   1  \\\\\n",
    "\\end{matrix} \\right] =\n",
    "\\left[ \\begin{matrix}\n",
    "    w / s_x & 0 & -wt_x/s_x \\\\\n",
    "    0 & h / s_y & -ht_y/s_y \\\\\n",
    "    0 & 0 & 1\n",
    "\\end{matrix} \\right]\n",
    "$$\n",
    "\n",
    "that is, a translation, followed by a (possibly) anisotropic scaling, to $H^{-1}$, so that the image corners are kept inside $D_W$. In order to get the image corners tightly mapped into $D_W$, we may choose\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "    s_x &= \\max\\left\\{ x_{UL}, x_{BL}, x_{UR}, x_{BR} \\right\\} - \\min\\left\\{ x_{UL}, x_{BL}, x_{UR}, x_{BR} \\right\\} \\\\\n",
    "    s_y &= \\max\\left\\{ y_{UL}, y_{BL}, y_{UR}, y_{BR} \\right\\} - \\min\\left\\{ y_{UL}, y_{BL}, y_{UR}, y_{BR} \\right\\} \\\\\n",
    "    t_x &= \\min\\left\\{ x_{UL}, x_{BL}, x_{UR}, x_{BR} \\right\\} \\\\\n",
    "    t_y &= \\min\\left\\{ y_{UL}, y_{BL}, y_{UR}, y_{BR} \\right\\} \\\\\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "Finally, we may go back and forth from image to final world coordinates using the transformations $S\\,H^{-1}$ and\n",
    "$H\\,S^{-1}$, respectively.\n",
    "\n",
    "---\n",
    "\n",
    "> <a href=\"footnote1\">[1]</a> The $D_W$ set corresponds, precisely, to the patch of the world plane that we are getting from rectifying the original image."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pixel adjustment\n",
    "\n",
    "As of now, we just grab the closest pixel (nearest point filtering)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Future work"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Other image adjustments\n",
    "\n",
    "* Include whole image\n",
    "* Include only largest visible rectangle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bi-linear filtering\n",
    "\n",
    "We need four points to perform bi-linear filtering. So, if \n",
    "\n",
    "$$\n",
    "P = \\left\\{ \n",
    "    (0, 0)^\\intercal, \n",
    "    (0, 1)^\\intercal, \n",
    "    (1, 0)^\\intercal, \n",
    "    (1, 1)^\\intercal \n",
    "\\right\\}\n",
    "$$\n",
    "\n",
    "we interpolate $f$ at $(x, y)$ with\n",
    "\n",
    "$$\n",
    "f(x, y) = \n",
    "x\\,y\\,z_{00} +\n",
    "x\\,(1-y)\\,z_{01} +\n",
    "(1-x)\\,y\\,z_{10} +\n",
    "(1-x)\\,(1-y)\\,z_{11} +\n",
    "$$\n",
    "\n",
    "considering that $z_{ij} = f(i,j)$ and that $(x,y)^\\intercal$ is already normalized to the $\\left[0,1\\right]$ interval."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bi-cubic filtering\n",
    "\n",
    "To do..."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  },
  "toc": {
   "toc_cell": false,
   "toc_number_sections": true,
   "toc_threshold": "3",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
